﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Algorithm.Search;
using Algorithm.Sort;

namespace Algorithm
{
    class Program
    {
        static void Main(string[] args)
        {
            InputOutputData io = new InputOutputData();
            while (true)
            {
                // Display menu, and request
                var key = io.Menu();
                switch (key)
                {
                    // Linear search
                    case SearchType.LinearSearch:
                        LinearSearch lnS = new LinearSearch();
                        lnS.Search();
                        break;
                    // Binary search
                    case SearchType.BinarySearch:
                        BinarySearch bnrS = new BinarySearch();
                        bnrS.Search();
                        break;
                    // Bubble sort
                    case SortType.BubbleSort:
                        BubbleSort bbSort = new BubbleSort();
                        bbSort.Sort();
                        break;
                    // Selection Sort
                    case SortType.SelectionSort:
                        SelectionSort sltSort = new SelectionSort();
                        sltSort.Sort();
                        break;
                    // Default
                    default:
                        break;
                }
                Console.ReadKey();
                Console.Clear();
            }
        }
    }
}
