﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithm.Sort
{
    class BubbleSort
    {
        /// <summary>
        /// Bubble sort
        /// </summary>
        public void Sort()
        {
            InputOutputData io = new InputOutputData();
            var data = io.InputListData();

            BubbleSortAlgorithm(data);

            Console.WriteLine("\n>>>>>>>>>> Sorted!!!");
            io.OutputListData(data);
        }

        #region Private
        /// <summary>
        /// Algorithm Bubble sort
        /// </summary>
        /// <param name="data">List data</param>
        private void BubbleSortAlgorithm(List<int> data)
        {
            var countData = data.Count;

            for (int countDataNew = countData - 1; countDataNew >= 1; countDataNew--)
            {
                for (int j = 0; j < countDataNew; j++)
                {
                    if (data[j] > data[j + 1])
                    {
                        Swap(data, j, j + 1);
                    }
                }
            }
        }

        /// <summary>
        /// Swap position of items in list
        /// </summary>
        /// <param name="data">List data</param>
        /// <param name="idx1">Position of item 1</param>
        /// <param name="idx2">Position of item 2</param>
        private void Swap(List<int> data, int idx1, int idx2)
        {
            var temp = data[idx1];
            data[idx1] = data[idx2];
            data[idx2] = temp;
        }
        #endregion
    }
}
