﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Algorithm.Sort
{
    class SelectionSort
    {
        /// <summary>
        /// Selection sort
        /// </summary>
        public void Sort()
        {
            InputOutputData io = new InputOutputData();
            var lstData = io.InputListData();

            SelectionSortAlgorithm(lstData);

            Console.WriteLine("\n>>>>> Sort!!!");
            io.OutputListData(lstData);            
        }

        #region Private
        /// <summary>
        ///  Algorithm Selection sort
        /// </summary>
        /// <param name="lstData">List data</param>
        private void SelectionSortAlgorithm(List<int> lstData)
        {
            for (int newIdx = 0; newIdx < lstData.Count; newIdx++)
            {
                int min = newIdx;

                for (int j = newIdx + 1; j < lstData.Count; j++)
                {
                    if(lstData[min] > lstData[j])
                    {
                        min = j;
                    }
                }

                if (min != newIdx)
                {
                    Swap(lstData, newIdx, min);
                }
            }
        }

        /// <summary>
        /// Swap position of items in list
        /// </summary>
        /// <param name="data">List data</param>
        /// <param name="idx1">Position of item 1</param>
        /// <param name="idx2">Position of item 2</param>
        private void Swap(List<int> data, int idx1, int idx2)
        {
            var temp = data[idx1];
            data[idx1] = data[idx2];
            data[idx2] = temp;
        }
        #endregion
    }
}
