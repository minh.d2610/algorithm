﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithm.Search
{
    class LinearSearch
    {
        /// <summary>
        /// Linear search
        /// </summary>
        public void Search()
        {
            InputOutputData io = new InputOutputData();
            var lstData = io.InputListData();
            var input = io.InputDataSearch();

            // Algorithm
            for (int i = 0; i < lstData.Count; i++)
            {
                if (lstData[i] == input)
                {
                    Console.Write($"\n>>> Found value at position: {i + 1}");
                    return;
                }
            }
            Console.WriteLine("\n>>> Not Found!!!");
        }

    }
}
