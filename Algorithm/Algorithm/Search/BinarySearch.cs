﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Algorithm.Search
{
    class BinarySearch
    {
        /// <summary>
        /// Binary search
        /// </summary>
        public void Search()
        {
            InputOutputData io = new InputOutputData();
            var lstData = io.InputListData().ToArray();
            var input = io.InputDataSearch();

            // Algorithm
            var result = BinarySearchAlgorithm(lstData, 0, lstData.Length, input);

            if (result == -1)
            {
                Console.Write($"\n>>> Not found!!!");
            }
            else
            {
                Console.Write($"\n>>> Found value at position: {result + 1}");
            }
        }

        #region Private
        /// <summary>
        /// Search: Binary search
        /// </summary>
        /// <param name="lstData">List data</param>
        /// <param name="start">Start index</param>
        /// <param name="end">End index</param>
        /// <param name="input">Value search</param>
        /// <returns>Position of value search</returns>
        private int BinarySearchAlgorithm(int[] lstData, int start, int end, int input)
        {
            if (start > end) return -1;

            // Get middle index
            int middle = (start + end) / 2;

            // If input exists in middle, return position, else -> recursion 
            if (input == lstData[middle])
            {
                return middle;
            }
            else if (input < lstData[middle])
            {
                return BinarySearchAlgorithm(lstData, start, middle - 1, input);
            }
            else
            {
                return BinarySearchAlgorithm(lstData, middle + 1, end, input);
            }
        }
        #endregion
    }
}
