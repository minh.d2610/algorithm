﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Algorithm
{
    class InputOutputData
    {
        /// <summary>
        /// Create menu
        /// </summary>
        /// <returns>Menu</returns>
        public string Menu()
        {
            string subject          = "Choose algorithm:\n";
            string search           = "\n----------SEARCH----------\n";
            string linearSeach      = "1. Linear search\n";
            string binarySearch     = "2. Binary search\n";
            string sort             = "\n-----------SORT-----------\n";
            string bubbleSort       = "3. Bubble sort\n";
            string selectionSort    = "4. Selection sort\n";
            string end              = "\n>>>>> Please choose [1~4]: ";
            Console.Write($"{subject}{search}{linearSeach}{binarySearch}{sort}{bubbleSort}{selectionSort}{end}");

            var key = Console.ReadLine();
            while (!IsNumber(key))
            {
                Console.Write($"{end}");
                key = Console.ReadLine();
            }

            Console.Clear();
            return key;
        }

        /// <summary>
        /// Input list data 
        /// </summary>
        /// <returns>List data</returns>
        public List<int> InputListData()
        {
            List<int> data = new List<int>();

            // Input data
            Console.Write("Input number of elements: ");
            string strLength = Console.ReadLine();
            while (!IsNumber(strLength))
            {
                Console.WriteLine("\n>>> Not match!!!");
                Console.Write("\n>>> Input number of elements: ");
                strLength = Console.ReadLine();
            }

            int length = int.Parse(strLength);
            for (int i = 0; i < length; i++)
            {
                Console.Write($"Input element {i + 1}: ");

                var value = Console.ReadLine();
                while (!IsNumber(value))
                {
                    Console.Write($"Please input value for element {i + 1} (Type: Number): ");
                    value = Console.ReadLine();
                }
                
                 data.Add(int.Parse(value));
            }

            Console.WriteLine("\nInput done!!!!");
            OutputListData(data);

            return data;
        }

        /// <summary>
        /// Input data search
        /// </summary>
        /// <returns></returns>
        public int InputDataSearch()
        {
            // Input data search
            Console.Write("\nEnter number to search: ");
            var strInput = Console.ReadLine();
            while (!IsNumber(strInput))
            {
                Console.WriteLine("\n>>> Not match!!!");
                Console.Write("\n>>> Enter number to search: ");
                strInput = Console.ReadLine();
            }

            return int.Parse(strInput);
        }

        /// <summary>
        /// Output list data
        /// </summary>
        /// <param name="data">List data</param>
        public void OutputListData(List<int> data)
        {
            // Print data input
            string breakLine = "-----------------------------------";

            
            string outputFormat = "List data: [{0}]";

            string result = string.Empty;
            foreach (var item in data)
            {
                result += $"{item}, "; 
            }

            result = (!string.IsNullOrEmpty(result)) ? result.Remove(result.Length - 2, 2): result;

            result = string.Format(outputFormat, result);

            Console.WriteLine(breakLine);
            Console.WriteLine(result);
            Console.WriteLine(breakLine);
        }

        #region Private
        /// <summary>
        /// Check type
        /// </summary>
        /// <param name="value">Value</param>
        /// <returns>Is number? (true/false)</returns>
        private bool IsNumber(string value)
        {
            // Check null
            if (string.IsNullOrEmpty(value))
            {
                return false;
            }

            // Check number
            if (!Regex.IsMatch(value, @"^\d+$"))
            {
                return false;
            }

            return true;
        }
        #endregion
    }
}
