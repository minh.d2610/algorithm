﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithm
{
    public class SearchType
    {
        public const string LinearSearch = "1";
        public const string BinarySearch = "2";
    }

    public class SortType
    {
        public const string BubbleSort = "3";
        public const string SelectionSort = "4";
    }
}
